<?php

use liberty_code\di\provider\api\ProviderInterface;
use people_sdk\library\requisition\request\info\factory\api\SndInfoFactoryInterface;
use people_sdk\param\param\model\ParamEntityFactory;
use people_sdk\param\param\model\repository\ParamEntitySimpleRepository;
use people_sdk\module_param\param\helper\ParamHelper;



return array(
    // Param entity services
    // ******************************************************************************

    'people_param_entity_factory' => [
        'source' => ParamEntityFactory::class,
        'argument' => [
            ['type' => 'class', 'value' => ProviderInterface::class]
        ],
        'option' => [
            'shared' => true
        ]
    ],

    'people_param_entity_simple_repository' => [
        'source' => ParamEntitySimpleRepository::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_requisition_requester'],
            ['type' => 'dependency', 'value' => 'people_requisition_persistor'],
            ['type' => 'class', 'value' => SndInfoFactoryInterface::class]
        ],
        'option' => [
            'shared' => true,
        ]
    ],



    // Param helper services
    // ******************************************************************************

    'people_param_helper' => [
        'source' => ParamHelper::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_param_entity_simple_repository']
        ],
        'option' => [
            'shared' => true
        ]
    ]
);