<?php
/**
 * Description :
 * This class allows to define param helper class.
 * Param helper allows to provide features,
 * for param.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\module_param\param\helper;

use liberty_code\library\bean\model\DefaultBean;

use people_sdk\param\param\library\ToolBoxParam;
use people_sdk\param\param\model\ParamEntity;
use people_sdk\param\param\model\repository\ParamEntitySimpleRepository;



class ParamHelper extends DefaultBean
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * DI: Param entity simple repository instance.
     * @var ParamEntitySimpleRepository
     */
    protected $objParamEntitySimpleRepository;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param ParamEntitySimpleRepository $objParamEntitySimpleRepository
     */
    public function __construct(
        ParamEntitySimpleRepository $objParamEntitySimpleRepository
    )
    {
        // Init properties
        $this->objParamEntitySimpleRepository = $objParamEntitySimpleRepository;

        // Call parent constructor
        parent::__construct();
    }





    // Methods getters
    // ******************************************************************************

    /**
     * Get index array of scoped attribute keys,
     * for scope get,
     * from specified param entity.
     *
     * Param entity simple repository execution configuration array format:
     * @see ToolBoxParam::getTabScopeGetAttrKey() param entity simple repository execution configuration array format.
     *
     * @param ParamEntity $objParamEntity
     * @param null|array $tabParamEntitySimpleRepoExecConfig = null
     * @return null|array
     */
    public function getTabScopeGetAttrKey(
        ParamEntity $objParamEntity,
        array $tabParamEntitySimpleRepoExecConfig = null
    )
    {
        // Return result
        return ToolBoxParam::getTabScopeGetAttrKey(
            $this->objParamEntitySimpleRepository,
            $objParamEntity,
            $tabParamEntitySimpleRepoExecConfig
        );
    }



    /**
     * Get index array of scoped attribute keys,
     * for scope update,
     * from specified param entity.
     *
     * Param entity simple repository execution configuration array format:
     * @see ToolBoxParam::getTabScopeUpdateAttrKey() param entity simple repository execution configuration array format.
     *
     * @param ParamEntity $objParamEntity
     * @param null|array $tabParamEntitySimpleRepoExecConfig = null
     * @return null|array
     */
    public function getTabScopeUpdateAttrKey(
        ParamEntity $objParamEntity,
        array $tabParamEntitySimpleRepoExecConfig = null
    )
    {
        // Return result
        return ToolBoxParam::getTabScopeUpdateAttrKey(
            $this->objParamEntitySimpleRepository,
            $objParamEntity,
            $tabParamEntitySimpleRepoExecConfig
        );
    }



}